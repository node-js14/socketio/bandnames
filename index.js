// Paquete para leer y establecer las variables de entorno
require('dotenv').config();

// Server Model: Contiene todo el servidor de express + socket.io configurado
const Servers = require('./server/server');
// const Server = require('./server/server');

// Inicializar la instancia del server
const server = new Servers();

// Ejecutar el server
server.execute();
