const Band = require('./band');

class BandList {
  constructor() {
    this.bands = [
      new Band('rock'),
      new Band('electro'),
      new Band('tecno'),
      new Band('cumbia'),
    ];
  }
  //  TODO agregando un elemento
  addBand(name) {
    const newBand = new Band(name);
    this.bands.push(newBand);
    return this.bands;
  }

  //  TODO removiendo un elemento
  removedBand(id) {
    this.bands = this.bands.filter((item) => item.id !== id);
  }

  //  TODO obteniendo toda la lista
  getBand() {
    return this.bands;
  }

  //  TODO incrementando votos
  increaseVotes(id) {
    this.bands = this.bands.map((item) => {
      if (item.id === id) {
        item.votes += 1;
      }
      return item;
    });
  }
  //   TODO cambiar un nombre
  changeName(id, name) {
    this.bands = this.bands.map((item) => {
      if (item.id === id) {
        item.name === name;
      }
      return item;
    });
  }
}

module.exports = BandList;
