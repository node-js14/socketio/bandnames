const BandList = require('../models/band-list');

class Sockets {
  constructor(io) {
    this.io = io;
    this.bandList = new BandList();
    this.socketEvents();
  }
  // console.log(io);
  socketEvents() {
    // console.log(this.io.on());
    //  TODO On connection
    this.io.on('connection', (socket) => {
      //  TODO Escuchar evento: mensaje-to-server
      console.log('cliente conectado');
      // console.log(`${socket.id}`);

      //  TODO emitir al cliente conectado todas las bandas
      socket.broadcast.emit('current-bands', this.bandList.getBand());

      // TODO escuchar al cliente conectado
      socket.on('votar-banda', (id) => {
        this.bandList.increaseVotes(id);
        socket.broadcast.emit('current-bands', this.bandList.getBand());
      });

      // TODO escuchar al cliente conectado
      socket.on('delete-band', (id) => {
        this.bandList.removedBand(id);
        socket.broadcast.emit('current-bands', this.bandList.getBand());
      });

      // TODO escuchar al cliente conectado
      socket.on('newName-band', ({ id, name }) => {
        this.bandList.changeName(id, name);
        socket.broadcast.emit('current-bands', this.bandList.getBand());
      });

      // TODO escuchar al cliente conectado
      socket.on('addNew-band', ({ name }) => {
        this.bandList.addBand(name);
        socket.broadcast.emit('current-bands', this.bandList.getBand());
      });
    });
  }
}

module.exports = Sockets;
