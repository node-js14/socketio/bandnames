// Servidor de Express
const express = require('express');
const { createServer } = require('http');
const { Server } = require('socket.io');
const path = require('path');
const cors = require('cors');
const Sockets = require('./sockets');
// const Sockets = require('./sockets');

class Servers {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;

    // Http server
    this.httpServer = createServer(this.app);

    // Configuraciones de sockets
    this.io = new Server(this.httpServer, {
      /* configuraciones */
      cors: {
        origins: 'http://localhost:8080',
        methods: ['GET', 'POST', 'PUT', 'DELETE'],
        //  header: 'Access-Control-Allow-Origin',
      },
    });
  }

  middlewares() {
    // Desplegar el directorio público
    this.app.use(express.static(path.resolve(__dirname, '../public')));

    // CORS
    this.app.use(cors());
  }

  // Esta configuración se puede tener aquí o como propieda de clase
  // depende mucho de lo que necesites
  configurarSockets() {
    new Sockets(this.io);
  }

  execute() {
    // Inicializar Middlewares
    this.middlewares();

    // Inicializar sockets
    this.configurarSockets();

    // Inicializar Server
    this.httpServer.listen(this.port, () => {
      console.log(`http://localhost:${this.port}`);
    });
  }
}

module.exports = Servers;
